import ProfileCard from "../../components/profile-card";
import "./index.style.scss";

const ProfileCardPage = () => {
  return (
    <div className="profile-page">
      <ProfileCard />
    </div>
  );
};
export default ProfileCardPage;
