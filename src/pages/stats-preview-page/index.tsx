import StatsPreviewCard from "../../components/stats-preview-card";
import "./index.style.scss";

const StatPreviewPage = () => {
  return (
    <div className="stats-page">
      <StatsPreviewCard />
    </div>
  );
};
export default StatPreviewPage;
