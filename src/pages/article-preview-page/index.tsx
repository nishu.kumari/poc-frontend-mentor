import ArticlePreviewCard from "../../components/article-preview-card";
import "./index.style.scss";

const ArticlePreviewPage = () => {
  return (
    <div className="article-page">
      <ArticlePreviewCard />
    </div>
  );
};
export default ArticlePreviewPage;
