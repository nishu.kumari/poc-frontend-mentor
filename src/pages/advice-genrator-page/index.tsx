import AdviceCard from "../../components/advice-card";
import "./index.style.scss";

const AdvicePage = () => {
  return (
    <div className="advice-page">
      <AdviceCard />
    </div>
  );
};
export default AdvicePage;
