import ReactChartComponent from "../../components/react-chart-component";
import "./index.style.scss";

const ChartPage = () => {
  return (
    <div className="chart-page">
      <ReactChartComponent />
    </div>
  );
};
export default ChartPage;
