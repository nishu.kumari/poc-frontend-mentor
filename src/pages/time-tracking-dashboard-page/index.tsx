import TimeTrackerComponent from "../../components/time-tracker-component";
import "./index.style.scss";

const TimeTrackerDashboardPage = () => {
  return (
    <div className="time-page">
      <TimeTrackerComponent />
    </div>
  );
};
export default TimeTrackerDashboardPage;
