import "./index.style.scss";
import Logo from "../../assets/images/logo.svg";
import Chevron from "../../assets/images/icon-arrow.svg";
import { useState } from "react";
import Person from "../../assets/images/hero-desktop.jpg";

const ComingSoonPage = () => {
  const [email, setEmail] = useState("");
  const [error, setError] = useState("");

  const handleSubmit = (e: any) => {
    e.preventDefault();
    if (/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(email)) {
      alert("submitted");
    } else {
      setError("Invalid Email");
    }
  };

  const handleChange = (e: any) => {
    setError("");
    setEmail(e.target.value);
  };

  return (
    <div className="coming-soon-page">
      <div className="logo">
        <img src={Logo} alt="logo" />
      </div>
      <div className="left-section">
        <div className="header">
          <span className="first">WE'RE</span>
          <p> COMING</p> SOON
        </div>
        <div className="paragraph">
          Hello fellow shoppers! We're currently building our new fashion store.
          Add your email below to stay up-to-date with announcements and our
          launch deals.
        </div>

        <div className="input-box">
          <input
            type="email"
            value={email}
            placeholder="Email Address"
            onChange={handleChange}
          />
          {error && <p className="error">{error}</p>}
          <button type="button" className="btn-submit" onClick={handleSubmit}>
            <img src={Chevron} alt="chevron" />
          </button>
        </div>
      </div>
      <div className="right-section">
        <img className="person-img" src={Person} alt="person" />
      </div>
    </div>
  );
};
export default ComingSoonPage;
