import OrderSummaryCard from "../../components/order-summary-card";
import "./index.style.scss";

const OrderSummaryPage = () => {
  return (
    <div className="product-page">
      <OrderSummaryCard />
    </div>
  );
};
export default OrderSummaryPage;
