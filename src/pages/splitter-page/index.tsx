import SplitterCard from "../../components/splitter-card";
import "./index.style.scss";

const SplitterPage = () => {
  return (
    <div className="splitter-page">
      <p className="title">SPLI</p>
      <p className="title">TTER</p>
      <SplitterCard />
    </div>
  );
};
export default SplitterPage;
