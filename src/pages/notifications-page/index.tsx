import NotificationComponent from "../../components/notification-component";
import "./index.style.scss";

const NotificationPage = () => {
  return (
    <div className="notifications-page">
      <NotificationComponent />
    </div>
  );
};
export default NotificationPage;
