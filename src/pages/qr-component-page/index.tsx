import QRComponent from "../../components/qr-component";
import "./index.style.scss";

const QRPage = () => {
  return (
    <div className="qr-page">
      <QRComponent />
    </div>
  );
};
export default QRPage;
