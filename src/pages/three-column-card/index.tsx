import "./index.style.scss";
import Luxury from "../../assets/images/icon-luxury.svg";
import SUV from "../../assets/images/icon-suvs.svg";
import Sedan from "../../assets/images/icon-sedans.svg";

const ThreeColumnCardPreview = () => {
  return (
    <div className="columns">
      <div className="column card-column">
        <div className="card card-sedan">
          <img className="card-img" src={Sedan} alt="img" />
          <h2 className="card-title">Sedans</h2>
          <p className="card-description">
            Choose a sedan for its affordability and excellent fuel economy.
            Ideal for cruising in the city or on your next road trip.
          </p>
          <div className="card-btn">Learn More</div>
        </div>
      </div>
      <div className="column card-column">
        <div className="card card-suv">
          <img className="card-img" src={SUV} alt="img" />
          <h2 className="card-title">SUVs</h2>
          <p className="card-description">
            Take an SUV for its spacious interior, power, and versatility.
            Perfect for your next family vacation and off-road adventures.
          </p>
          <div className="card-btn">Learn More</div>
        </div>
      </div>
      <div className="column card-column">
        <div className="card card-luxury">
          <img className="card-img" src={Luxury} alt="img" />
          <h2 className="card-title">Luxury</h2>
          <p className="card-description">
            Cruise in the best car brands without the bloated prices. Enjoy the
            enhanced comfort of a luxury rental and arrive in style.
          </p>
          <div className="card-btn">Learn More</div>
        </div>
      </div>
    </div>
  );
};
export default ThreeColumnCardPreview;
