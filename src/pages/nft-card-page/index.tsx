import NFTCard from "../../components/nft-card";
import "./index.style.scss";

const NFTPage = () => {
  return (
    <div className="nft-page">
      <NFTCard />
    </div>
  );
};
export default NFTPage;
