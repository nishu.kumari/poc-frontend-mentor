import "./index.style.scss";
import Facebook from "../../assets/images/facebook.svg";
import Pinterest from "../../assets/images/pinterest.svg";
import Instagram from "../../assets/images/icon-instagram.svg";
import { useCountdown } from "./useCountdown";

const CountdownTimerPage = () => {
  const [days, hours, minutes, seconds] = useCountdown(new Date(2022, 11, 16));
  const isValid = days + hours + minutes + seconds >= 0;
  return (
    <div className="countdown-page">
      {isValid ? (
        <>
          <div className="title">WE'RE LAUNCHING SOON</div>
          <div className="timer-wrapper">
            <div className="single-timer">
              <div className="time">
                <p>{days}</p>
              </div>
              <div className="placeholder">DAYS</div>
            </div>
            <div className="single-timer">
              <div className="time">{hours}</div>
              <div className="placeholder">HOURS</div>
            </div>
            <div className="single-timer">
              <div className="time">{minutes}</div>
              <div className="placeholder">MINUTES</div>
            </div>
            <div className="single-timer">
              <div className="time">{seconds}</div>
              <div className="placeholder">SECONDS</div>
            </div>
          </div>
        </>
      ) : (
        <p> Expired</p>
      )}

      <div className="footer">
        <img src={Facebook} alt="fb" />
        <img src={Pinterest} alt="pinterest" />
        <img src={Instagram} alt="instagram" />
      </div>
    </div>
  );
};

export default CountdownTimerPage;
