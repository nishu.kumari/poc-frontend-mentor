import "./index.style.scss";
import Person1 from "../../assets/images/image-daniel.jpg";
import Person2 from "../../assets/images/image-jonathan.jpg";
import Person3 from "../../assets/images/image-jeanette.jpg";
import Person4 from "../../assets/images/image-patrick.jpg";
import Person5 from "../../assets/images/image-kira.jpg";

const TestimonialGrid = () => {
  return (
    <div className="testimonial-card">
      <div className="grid-container">
        <div className="item1">
          <div className="card">
            <div className="card-header">
              <img src={Person1} alt="img" />
              <div className="profile">
                <div className="name">David Cliffered</div>
                <div className="designation">Verified Graduate</div>
              </div>
            </div>
            <div className="title">
              I received a job offer mid course and the subjects I learned were
              current, If not more so. In the company I joined. I honestly feel
              I got every penny's worth.
            </div>
            <div className="description">
              “ I was an EMT for many years before I joined the bootcamp. I’ve
              been looking to make a transition and have heard some people who
              had an amazing experience here. I signed up for the free intro
              course and found it incredibly fun! I enrolled shortly thereafter.
              The next 12 weeks was the best - and most grueling - time of my
              life. Since completing the course, I’ve successfully switched
              careers, working as a Software Engineer at a VR startup. ”
            </div>
          </div>
        </div>
        <div className="item2">
          <div className="card">
            <div className="card-header">
              <img src={Person2} alt="img" />
              <div className="profile">
                <div className="name">Jonathan Walters</div>
                <div className="designation">Verified Graduate</div>
              </div>
            </div>
            <div className="title">
              The team was very supportive and kept me motivated
            </div>
            <div className="description">
              "I started as a total newbie with virtually no coding skills. I
              now work as a mobile engineer for a big company. This was one of
              the best investments I'hv made in myself."
            </div>
          </div>
        </div>
        <div className="item3">
          <div className="card">
            <div className="card-header">
              <img src={Person3} alt="img" />
              <div className="profile">
                <div className="name">Jeanette Harmon</div>
                <div className="designation">Verified Graduate</div>
              </div>
            </div>
            <div className="title">
              An overall wonderful and rewarding experience
            </div>
            <div className="description">
              “ Thank you for the wonderful experience! I now have a job I
              really enjoy, and make a good living while doing something I love.
              ”
            </div>
          </div>
        </div>
        <div className="item4">
          <div className="card">
            <div className="card-header">
              <img src={Person4} alt="img" />
              <div className="profile">
                <div className="name">Patrick Abrams</div>
                <div className="designation">Verified Graduate</div>
              </div>
            </div>
            <div className="title">
              Awesome teaching support from TAs who did the bootcamp themselves.
              Getting guidance from them and learning from their experiences was
              easy.
            </div>
            <div className="description">
              “ The staff seem genuinely concerned about my progress which I
              find really refreshing. The program gave me the confidence
              necessary to be able to go out in the world and present myself as
              a capable junior developer. The standard is above the rest. You
              will get the personal attention you need from an incredible
              community of smart and amazing people. ”
            </div>
          </div>
        </div>
        <div className="item5">
          <div className="card">
            <div className="card-header">
              <img src={Person5} alt="img" />
              <div className="profile">
                <div className="name"> Kira Whittle</div>
                <div className="designation">Verified Graduate</div>
              </div>
            </div>
            <div className="title">
              Such a life-changing experience. Highly recommended!
            </div>
            <div className="description">
              “ Before joining the bootcamp, I’ve never written a line of code.
              I needed some structure from professionals who can help me learn
              programming step by step. I was encouraged to enroll by a former
              student of theirs who can only say wonderful things about the
              program. The entire curriculum and staff did not disappoint. They
              were very hands-on and I never had to wait long for assistance.
              The agile team project, in particular, was outstanding. It took my
              learning to the next level in a way that no tutorial could ever
              have. In fact, I’ve often referred to it during interviews as an
              example of my developent experience. It certainly helped me land a
              job as a full-stack developer after receiving multiple offers.
              100% recommend! ”
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default TestimonialGrid;
