import ProductCard from "../../components/product-card";
import "./index.style.scss";

const ProductCardPage = () => {
  return (
    <div className="product-page">
      <ProductCard />
    </div>
  );
};
export default ProductCardPage;
