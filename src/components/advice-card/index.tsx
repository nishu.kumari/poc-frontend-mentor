import "./index.style.scss";
import Dice from "../../assets/images/icon-dice.svg";
import Divider from "../../assets/images/pattern-divider-desktop.svg";
import { useEffect, useState } from "react";
import axios from "axios";

const AdviceCard = () => {
  const [advice, setAdvice] = useState({ id: "0", description: "" });
  async function showQuote() {
    const data = await axios.get("https://api.adviceslip.com/advice");
    setAdvice({ id: data.data.slip.id, description: data.data.slip.advice });
  }

  useEffect(() => {
    showQuote();
  }, []);

  return (
    <div className="card">
      <p className="tag">ADVICE #{advice.id}</p>
      <p className="description">{`"${advice.description}."`}</p>
      <img src={Divider} alt="divider" className="divider" />
      <div className="dice" onClick={showQuote}>
        <img src={Dice} alt="dice" />
      </div>
    </div>
  );
};
export default AdviceCard;
