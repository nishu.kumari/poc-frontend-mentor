import "./index.style.scss";
import ArticleCardImage from "../../assets/images/drawers.jpg";
import Avatar from "../../assets/images/avatar-michelle.jpg";
import Share from "../../assets/images/icon-share.svg";
import Facebook from "../../assets/images/icon-facebook.svg";
import Pinterest from "../../assets/images/icon-pinterest.svg";
import Twitter from "../../assets/images/icon-twitter.svg";
import { useState } from "react";

const ArticlePreviewCard = () => {
  const [isPopupOpen, togglePopup] = useState<boolean>(false);

  return (
    <div className="article-preview-card">
      <img
        src={ArticleCardImage}
        alt="profile"
        className="article-preview-image"
      />

      <div className="card-content">
        <p className="primary-text">
          Shift the overall look and feel by adding these wonderful touches to
          furniture your home
        </p>
        <p className="secondary-text">
          Ever been in a room and felt like something was missing? Perhaps it
          felt slightly bare and uninviting. I've got some simple tips to help
          you make any room feel complete.
        </p>
        <div className="footer">
          <div className="profile">
            <img src={Avatar} alt="avatar" className="avatar" />
            <div className="detail">
              <p className="name">Michelle Appleton</p>
              <p className="date">28 Jun 2020</p>
            </div>
          </div>
          <div
            onClick={() => togglePopup(!isPopupOpen)}
            className={isPopupOpen ? "share-icon open" : "share-icon"}
          >
            <img src={Share} alt="share" className="share" />
            {isPopupOpen && (
              <div className="social-popup">
                <p className="share-text">SHARE</p>
                <img src={Facebook} alt="fb" />
                <img src={Twitter} alt="twitter" />
                <img src={Pinterest} alt="pinterest" />
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};
export default ArticlePreviewCard;
