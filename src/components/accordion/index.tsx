import "./index.style.scss";
import React from "react";
import Illustration from "../../assets/images/illustration-woman-online-desktop.svg";
import Pattern from "../../assets/images/bg-pattern-desktop.svg";
import Box from "../../assets/images/illustration-box-desktop.svg";
import AccordionItem from "../accordion-item";
const Accordion = (props: any) => {
  const { title, accordionData } = props;

  return (
    <div className="accordion-wrapper">
      <div className="left-section">
        <div className="images">
          <img src={Pattern} alt="illustration" className="img-pattern" />
          <img
            src={Illustration}
            alt="illustration"
            className="img-illustration"
          />
        </div>
        <img src={Box} alt="illustration" className="img-box" />
      </div>
      <div className="right-section">
        <div className="ac-title">{title}</div>
        <div className="ac-data">
          {accordionData?.map((data: any, index: number) => (
            <AccordionItem
              title={data.title}
              description={data.description}
              isOpen={index === 0}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default Accordion;
