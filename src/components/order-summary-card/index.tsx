import "./index.style.scss";
import Music from "../../assets/images/icon-music.svg";
import Illustration from "../../assets/images/illustration-hero.svg";

const OrderSummaryCard = () => {
  return (
    <div className="summary-card">
      <img src={Illustration} className="summary-image" alt="nft" />
      <div className="summary-card-content">
        <p className="primary-text">Order Summary</p>
        <p className="secondary-text">
          You can now listen to millions of songs, audiobooks, and podcasts on
          anywhere you like!
        </p>
        <div className="pricing">
          <img src={Music} alt="music" className="music" />
          <div className="plan">
            <p>Annual Plan</p>
            <p className="sub-text">$59.99/year</p>
          </div>
          <p className="link">Change</p>
        </div>
        <button className="primary-button">Proceed to Payment</button>
        <button className="secondary-button">Cancel Order</button>
      </div>
    </div>
  );
};
export default OrderSummaryCard;
