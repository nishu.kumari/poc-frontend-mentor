import "./index.style.scss";
import QRImage from "../../assets/images/image-qr-code.png";

const QRComponent = () => {
  return (
    <div className="qr-card">
      <img src={QRImage} className="qr-image" alt="qr" />
      <p className="primary-text">
        Improve your front-end skills by building projects
      </p>
      <p className="secondary-text">
        Scan the QR code to visit Frontend Mentor and take your coding skills to
        the next level
      </p>
    </div>
  );
};
export default QRComponent;
