import "./index.style.scss";
import StatsCardImage from "../../assets/images/image-header-desktop.jpg";

const StatsPreviewCard = () => {
  return (
    <div className="stats-preview-card">
      <div className="card-content">
        <p className="primary-text">
          Get <span>insights</span> that help your business grow.
        </p>
        <p className="secondary-text">
          Discover the benefits of data analytics and make better decisions
          regarding revenue, customer experience, and overall efficiency.
        </p>
        <div className="footer">
          <div className="stats">
            <p className="likes">10k+</p>
            <p className="text">COMPANIES</p>
          </div>
          <div className="stats">
            <p className="likes">314</p>
            <p className="text">TEMPLATES</p>
          </div>
          <div className="stats">
            <p className="likes">12M+</p>
            <p className="text">QUERIES</p>
          </div>
        </div>
      </div>
      <div className="stats-preview-image">
        <img src={StatsCardImage} alt="profile" />
      </div>
    </div>
  );
};
export default StatsPreviewCard;
