import "./index.style.scss";
import JSONData from "./data.json";
import Jeremy from "../../assets/images/image-jeremy.png";
import Work from "../../assets/images/icon-work.svg";
import Play from "../../assets/images/icon-play.svg";
import Ellipsis from "../../assets/images/icon-ellipsis.svg";
import { useEffect, useState } from "react";

interface IDataType {
  hours: number;
  lastTime: number;
  title: string;
}
const TimeTrackerComponent = () => {
  const [pageData, setPageData] = useState<IDataType[]>();
  const [timeSpan, setTimeSpan] = useState<string>("daily");

  const TypeOfTimeFrames = [...JSONData][0].timeframes;

  function fetchData(type: keyof typeof TypeOfTimeFrames) {
    const data = [...JSONData];
    const dailyData = data.map((item, index) => {
      const result = {
        title: item.title,
        lastTime: item.timeframes[type].previous,
        hours: item.timeframes[type].current,
      };
      return result;
    });
    setPageData(dailyData);
    setTimeSpan(type.toString());
  }

  useEffect(() => {
    fetchData("daily");
  }, []);

  return (
    <div className="container">
      <div className="report-wrapper-container">
        <div className="report-wrapper">
          <img src={Jeremy} alt="person" />
          <div>
            <p className="small-text hint">Report for</p>
            <p className="title">Jeremy Robson</p>
          </div>
        </div>

        <div className="time-option">
          <button
            id="daily-btn"
            className={timeSpan === "daily" ? "hint selected" : "hint"}
            onClick={() => fetchData("daily")}
          >
            Daily
          </button>
          <button
            id="weekly-btn"
            className={timeSpan === "weekly" ? "hint selected" : "hint"}
            onClick={() => fetchData("weekly")}
          >
            Weekly
          </button>
          <button
            id="monthly-btn"
            className={timeSpan === "monthly" ? "hint selected" : "hint"}
            onClick={() => fetchData("monthly")}
          >
            Monthly
          </button>
        </div>
      </div>

      <div className="reports-container">
        {pageData?.map((item: any, index: number) => (
          <div className={item.title.toLowerCase()} key={index}>
            <div className="card-image"></div>
            <div className="report-information">
              <div className="title">
                <p>{item.title}</p>
                <img src={Ellipsis} className="ellipsis" alt="ellipsis" />
              </div>
              <h2 className="hours">{item.hours} hrs</h2>
              <p className="last-time"> Last Week - {item.lastTime}hrs</p>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};
export default TimeTrackerComponent;
