import "./index.style.scss";
import PersonImage from "../../assets/images/image-victor.jpg";
import ProfileCardTop from "../../assets/images/bg-pattern-card.svg";

const ProfileCard = () => {
  return (
    <div className="profile-card">
      <img src={ProfileCardTop} className="profile-image" alt="profile" />
      <div className="card-content">
        <img className="avatar" src={PersonImage} alt="person" />
        <p className="primary-text">
          Victor Crest <span>26</span>
        </p>
        <p className="secondary-text">London</p>
        <div className="footer">
          <div className="stats">
            <p className="likes">80K</p>
            <p className="text">Followers</p>
          </div>
          <div className="stats">
            <p className="likes">80.3K</p>
            <p className="text">Likes</p>
          </div>
          <div className="stats">
            <p className="likes">1.4K</p>
            <p className="text">Photos</p>
          </div>
        </div>
      </div>
    </div>
  );
};
export default ProfileCard;
