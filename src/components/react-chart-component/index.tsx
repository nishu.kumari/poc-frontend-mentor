import "./index.style.scss";
import React, { useState } from "react";
import ChartLogo from "../../assets/images/chart-logo.svg";
import Data from "./data.json";
import {
  BarChart,
  Bar,
  XAxis,
  Legend,
  Tooltip,
  Cell,
  TooltipProps,
} from "recharts";
import {
  ValueType,
  NameType,
} from "recharts/types/component/DefaultTooltipContent";

const data = [...Data];

export default function ReactChartComponent() {
  const [activeIndex, setActiveIndex] = useState(-1);
  const handleHover = (data: any, index: number) => {
    setActiveIndex(index);
  };
  const CustomTooltip = ({
    active,
    payload,
    label,
  }: TooltipProps<ValueType, NameType>) => {
    if (active) {
      return (
        <div className="custom-tooltip">
          <p className="label">{`$${payload?.[0].value}`}</p>
        </div>
      );
    }

    return null;
  };

  return (
    <div className="chart-wrapper">
      <div className="header">
        <div className="content">
          <p className="text">My balance</p>
          <p className="balance">$921.48</p>
        </div>
        <img src={ChartLogo} alt="logo" />
      </div>
      <div className="chart-body">
        <p className="chart-body-title">Spending - Last 7 days</p>
        <BarChart
          width={360}
          height={200}
          data={data}
          margin={{
            right: 8,
          }}
        >
          <Bar
            dataKey="amount"
            fill="hsl(10, 79%, 65%)"
            radius={[5, 5, 5, 5]}
            onMouseEnter={handleHover}
            onMouseLeave={() => setActiveIndex(-1)}
          >
            {data.map((entry, index) => (
              <Cell
                cursor="pointer"
                opacity={index === activeIndex ? 0.7 : 1}
                fill={index === 2 ? "hsl(186, 34%, 60%)" : "hsl(10, 79%, 65%)"}
                key={`cell-${index}`}
              />
            ))}
          </Bar>
          <XAxis
            dataKey="day"
            tick={{ fontSize: 12 }}
            dy={10}
            axisLine={false}
            tickLine={false}
          />
          <Tooltip
            cursor={{ fill: "transparent" }}
            content={<CustomTooltip />}
            offset={-60}
          />
        </BarChart>
        <div className="chart-body-footer">
          <div className="total">
            <p className="text">Total this month</p>
            <p className="balance">$478.33</p>
          </div>
          <div className="stats">
            <p className="text">+2.4%</p>
            <p className="hint">from last month</p>
          </div>
        </div>
      </div>
    </div>
  );
}
