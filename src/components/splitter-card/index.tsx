import "./index.style.scss";
import Dollar from "../../assets/images/icon-dollar.svg";
import Person from "../../assets/images/icon-person.svg";
import { useState } from "react";

const SplitterCard = () => {
  const [billAmount, setBillAmount] = useState<string>("");
  const [calculatedData, setCalculatedData] = useState<any>({
    tipAmount: "0.00",
    total: "0.00",
  });
  const [billAmountError, setBillAmountError] = useState<string>("");
  const [numberOfPersonError, setNumberOfPersonError] = useState<string>("");
  const [numberOfPerson, setNumberOfPerson] = useState<string>("");

  const calculateTip = (event: any) => {
    console.log(numberOfPerson);
    const percentage = parseInt(event.target.getAttribute("data-percentage"));
    if (!billAmount) {
      setBillAmountError("Please Enter Bill Amount.");
    } else if (!numberOfPerson) {
      setNumberOfPersonError("Please Enter Number of Person.");
    } else if (
      parseInt(numberOfPerson) === 0 ||
      parseInt(numberOfPerson) !== parseFloat(numberOfPerson)
    ) {
      setNumberOfPersonError("Please Enter Valid Number of Person.");
    } else {
      const totalTip = parseFloat(billAmount) * (percentage / 100);
      const totalBill = totalTip + parseFloat(billAmount);
      setCalculatedData({
        tipAmount: (totalTip / parseInt(numberOfPerson)).toFixed(2),
        total: (totalBill / parseInt(numberOfPerson)).toFixed(2),
      });
    }
  };

  const handleBillChange = (e: any) => {
    setBillAmountError("");
    setBillAmount(e.target.value);
  };

  const handleNumberOfPersonChange = (e: any) => {
    setNumberOfPersonError("");
    setNumberOfPerson(Math.round(e.target.value.replace(/\D/g, "")).toString());
  };

  const handleReset = () => {
    setCalculatedData({
      tipAmount: "0.00",
      total: "0.00",
    });
    setBillAmount("");
    setNumberOfPerson("");
    setBillAmountError("");
    setNumberOfPersonError("");
  };

  return (
    <div className="splitter-card">
      <div className="left-section">
        <div className="input-field">
          <label className="label">Bill</label>
          <div className="input-group">
            <img src={Dollar} className="input-image" alt="dollar" />
            <input
              type="number"
              value={billAmount}
              onChange={handleBillChange}
              placeholder="0"
            />
          </div>
          {billAmountError && <p className="error">{billAmountError}</p>}
        </div>
        <div className="tip-options">
          <label className="label">Select Tip %</label>
          <div className="options">
            <div className="buttons" data-percentage={5} onClick={calculateTip}>
              5%
            </div>
            <div
              className="buttons"
              data-percentage={10}
              onClick={calculateTip}
            >
              10%
            </div>
            <div
              className="buttons"
              data-percentage={15}
              onClick={calculateTip}
            >
              15%
            </div>
            <div
              className="buttons"
              data-percentage={20}
              onClick={calculateTip}
            >
              20%
            </div>
            <div
              className="buttons"
              data-percentage={25}
              onClick={calculateTip}
            >
              25%
            </div>
            <div
              className="buttons"
              data-percentage={50}
              onClick={calculateTip}
            >
              50%
            </div>
          </div>
        </div>
        <div className="input-field">
          <label className="label">Number of People</label>
          <div className="input-group">
            <img src={Person} className="input-image" alt="dollar" />
            <input
              type="number"
              value={numberOfPerson}
              onChange={handleNumberOfPersonChange}
              placeholder="0"
            />
          </div>
          {numberOfPersonError && (
            <p className="error">{numberOfPersonError}</p>
          )}
        </div>
      </div>
      <div className="right-section">
        <div className="results">
          <div className="text">
            <p>Tip Amount</p>
            <p className="hint">/person</p>
          </div>
          <div className="price">{`$${calculatedData.tipAmount}`}</div>
        </div>
        <div className="results">
          <div className="text">
            <p>Total</p>
            <p className="hint">/person</p>
          </div>
          <div className="price">{`$${calculatedData.total}`}</div>
        </div>
        <button className="reset" type="button" onClick={handleReset}>
          RESET
        </button>
      </div>
    </div>
  );
};
export default SplitterCard;
