import "./index.style.scss";
import Data from "./data.json";

import Avatar from "../../assets/images/avatar-angela-gray.webp";
import { useEffect, useState } from "react";

const NotificationComponent = (props: any) => {
  const notifications = [...Data];

  const [messageState, setMessageState] = useState<boolean[]>([
    false,
    false,
    false,
    false,
    false,
    false,
    false,
  ]);

  useEffect(() => {
    setMessageState(notifications.map((item) => item.isRead));
  }, []);

  const markAllRead = () => {
    const updatedArray = [...messageState];
    updatedArray.fill(true);
    setMessageState(updatedArray);
  };

  return (
    <div className="notification-wrapper">
      <div className="header">
        <div className="title">
          Notifications
          <span>{messageState.filter((item) => item === false).length}</span>
        </div>
        <button
          type="button"
          className="link"
          disabled={messageState.filter((item) => item === false).length === 0}
          onClick={markAllRead}
        >
          Mark all as read
        </button>
      </div>
      <div className="notifications">
        {notifications.map((item: any, index: number) => (
          <div
            key={index}
            className={
              messageState[index]
                ? "single-notification"
                : "single-notification unread"
            }
            onClick={() => {
              if (messageState[index] === false) {
                const items = [...messageState];
                items[index] = true;
                setMessageState(items);
              }
            }}
          >
            <img src={Avatar} alt="img" />
            <div className="content">
              <p className="n-title">
                <span className="txt-bold">{item.user}</span>
                {` ${item.notification} `}
                <span
                  className={
                    item.referenceSrc !== "#" ? "txt-bold blue" : "txt-bold"
                  }
                >
                  {` ${item.reference} `}
                </span>
                {!messageState[index] && <span className="circle"></span>}
              </p>
              <span className="timeline">{item.timing}</span>
              {item.message && <div className="message">{item.message}</div>}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default NotificationComponent;
