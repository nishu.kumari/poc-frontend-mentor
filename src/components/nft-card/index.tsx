import "./index.style.scss";
import Etherum from "../../assets/images/icon-ethereum.svg";
import Clock from "../../assets/images/icon-clock.svg";
import Avatar from "../../assets/images/image-avatar.png";
import DiceImage from "../../assets/images/image-equilibrium.jpg";

const NFTCard = () => {
  return (
    <div className="nft-card">
      <img src={DiceImage} className="nft-image" alt="nft" />
      <p className="primary-text">Equilibrium #3429</p>
      <p className="secondary-text">
        Our Equilibrium collection promotes balance and calm.
      </p>
      <div className="pricing">
        <div className="eth">
          <img src={Etherum} alt="eth" />
          <p>0.041ETH</p>
        </div>
        <div className="timeline">
          <img src={Clock} alt="clock" />
          <p>3 days left</p>
        </div>
      </div>
      <div className="footer">
        <img src={Avatar} alt="avatar" className="avatar" />
        <p className="footer-text">
          Creation of <span className="name">Jules Wyvern</span>
        </p>
      </div>
    </div>
  );
};
export default NFTCard;
