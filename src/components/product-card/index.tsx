import "./index.style.scss";
import CartIcon from "../../assets/images/icon-cart.svg";
import ProductImage from "../../assets/images/product-desktop.jpg";

const ProductCard = () => {
  return (
    <div className="product-card">
      <div className="left-section">
        <img src={ProductImage} alt="product-img" />
      </div>
      <div className="right-section">
        <p className="product">PERFUME</p>
        <p className="product-name"> Gabrielle Essence Eau De Parfum</p>
        <p className="description">
          A floral, solar and voluptunous interpretation composed by Olivier
          Polge, Perfumer-Creator for the House of CHANEL
        </p>
        <div className="price-section">
          <p className="discounted-price">$149.99</p>
          <p className="actual-price">$169.99</p>
        </div>
        <button type="submit" className="cart-btn">
          <img src={CartIcon} alt="cart" />
          Add to Cart
        </button>
      </div>
    </div>
  );
};
export default ProductCard;
