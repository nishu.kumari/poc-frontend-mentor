import { useEffect, useState } from "react";
import ArrowUp from "../../assets/images/icon-arrow-down.svg";
import "./index.style.scss";

const AccordionItem = (props: any) => {
  const { title, description, isOpen } = props;
  const [isAccordionOpen, setAccordionState] = useState(false);
  const handleToggle = () => {
    setAccordionState(!isAccordionOpen);
  };

  useEffect(() => {
    if (isOpen) {
      setAccordionState(true);
    }
  }, [isOpen]);

  return (
    <div className="accordion-item-wrapper">
      <div
        className={isAccordionOpen ? "ac-item-title bold" : "ac-item-title"}
        onClick={handleToggle}
      >
        {title}
        <img
          className={isAccordionOpen ? "image open" : "image"}
          src={ArrowUp}
          alt="dropdown"
        />
      </div>

      {isAccordionOpen && (
        <div
          className={
            isAccordionOpen ? "ac-item-description-open" : "ac-item-description"
          }
        >
          {description}
        </div>
      )}
    </div>
  );
};
export default AccordionItem;
