import React from "react";
import "./App.style.scss";
import Accordion from "./components/accordion";
import ProductCard from "./components/product-card";
import ReactChartComponent from "./components/react-chart-component";
import AdvicePage from "./pages/advice-genrator-page";
import ArticlePreviewPage from "./pages/article-preview-page";
import ComingSoonPage from "./pages/coming-soon";
import CountdownTimerPage from "./pages/countdown-timer-page";
import NFTPage from "./pages/nft-card-page";
import NotificationPage from "./pages/notifications-page";
import OrderSummaryPage from "./pages/order-summary-page";
import ProductCardPage from "./pages/product-page";
import ProfileCardPage from "./pages/profile-card-page";
import QRPage from "./pages/qr-component-page";
import ChartPage from "./pages/react-chart-page";
import SplitterPage from "./pages/splitter-page";
import StatPreviewPage from "./pages/stats-preview-page";
import TestimonialGrid from "./pages/testimonial-grid";
import ThreeColumnCardPreview from "./pages/three-column-card";
import TimeTrackerDashboardPage from "./pages/time-tracking-dashboard-page";

const accordionData = [
  {
    title: "How many team members can invite?",
    description: "No limit as such for inviting team members",
  },
  {
    title: "What is the maximum file upload size?",
    description:
      "No more than 2GB files in your account must fit your alloted storage space.",
  },
  {
    title: "How many team members can invite?",
    description: "No limit as such for inviting team members",
  },
  {
    title: "What is the maximum file upload size?",
    description:
      "No more than 2GB files in your account must fit your alloted storage space.",
  },
  {
    title: "What is the maximum file upload size?",
    description:
      "No more than 2GB files in your account must fit your alloted storage space.",
  },
];

function App() {
  return (
    <>
      {/* <div className="page">
        <Accordion title="FAQ" accordionData={accordionData} />
      </div> */}
      {/* <ProductCardPage /> */}
      {/* <ComingSoonPage /> */}
      {/* <QRPage /> */}
      {/* <NFTPage /> */}
      {/* <OrderSummaryPage /> */}
      {/* <ProfileCardPage /> */}
      {/* <StatPreviewPage /> */}
      {/* <ArticlePreviewPage /> */}
      {/* <SplitterPage /> */}
      {/* <ThreeColumnCardPreview /> */}
      {/* <AdvicePage /> */}
      {/* <TimeTrackerDashboardPage /> */}
      {/* <CountdownTimerPage /> */}
      {/* <ChartPage /> */}
      {/* <NotificationPage /> */}
      <TestimonialGrid />
    </>
  );
}

export default App;
